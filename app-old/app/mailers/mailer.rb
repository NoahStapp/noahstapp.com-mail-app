class Mailer < ApplicationMailer
  default from: "noah@noahstapp.com"

  def pdf_email(user)
    @user = user
    mail(to: @user.email, subject: "Improving Your Business's Website")
  end
end
