# Preview all emails at http://localhost:3000/rails/mailers/mailer
class MailerPreview < ActionMailer::Preview
  def pdf_email_preview
    Mailer.pdf_email(User.first)
  end
end
